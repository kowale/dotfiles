" make backspace cool
set backspace=indent,eol,start

" encoding
set encoding=utf8

" mouse
set mouse=a

" use system clipboard
set clipboard=unnamedplus

" select everything
nnoremap <C-a> ggVG<CR>

" undo
set undolevels=300
set undofile
set undodir=~/.config/nvim/undo/
nmap <c-z> u
imap <c-z> <Esc>u

" moving lines
imap <C-Up> <Esc>ddkPi
nmap <C-Up> ddkP
imap <C-Down> <Esc>ddjPi
nmap <C-Down> ddjP

" copy and save
vnoremap <c-c> "*y
vnoremap <c-v> "*p
vnoremap <c-C> "+y
vnoremap <c-V> "+p

" esc to turn off search hl
nnoremap <silent> <esc> :noh<cr>

" multiline indents
vnoremap < <gv
vnoremap > >gv

" disable macro recording
nnoremap q <Nop>

" disable swaps and backups
set noswapfile
set nobackup
set nowritebackup

" dark colorscheme
colorscheme default
set termguicolors

" remove bottom bar
set laststatus=1

" tabs -> spaces
set softtabstop=4
set expandtab
set tabstop=4
set shiftwidth=4
set autoindent
set smartindent

" wrap at words
set linebreak

" display filename as title
set title

" english and polish spelling
set nospell spelllang=en_gb
nnoremap <silent> <F6> :setlocal spell! spelllang=en<cr>
inoremap <silent> <F9> <C-O>:setlocal spell! spelllang=pl<cr>
nnoremap <silent> <F9> :set invspell<cr>
inoremap <silent> <F9> <C-O>:set invspell<cr>

" ctrl-s to save (needs stty -ixon)
nnoremap <silent><c-s> :<c-u>update<cr>
inoremap <silent><c-s> <c-o>:update<cr>
vnoremap <silent><c-s> <c-c>:update<cr>gv

" ctrl-d to exit
nnoremap <silent><c-d> :<c-u>q<cr>
inoremap <silent><c-d> <c-o>:q<cr>
vnoremap <silent><c-d> <c-c>:q<cr>

" buffer presentations
" nnoremap <C-Right> :silent bn<CR> :redraw!<CR> gg
" nnoremap <C-Left> :silent bp<CR> :redraw!<CR> gg

" buffer specific settings
au BufNewFile,BufRead *.py
    \ set tabstop=4 |
    \ set softtabstop=4 |
    \ set shiftwidth=4 |
    \ set textwidth=79 |
    \ set expandtab |
    \ set autoindent |
    \ set fileformat=unix
au BufNewFile,BufRead *.md
    \ set textwidth=85 |
    \ set conceallevel=0

" ctrl+backspace deletes word before
inoremap <C-H> <C-w>

" completion settings
set completeopt=menuone,noinsert,noselect
set shortmess+=c
set wildmode=longest,list
" inoremap <Tab> <C-x><C-o>

" plugins
call plug#begin('~/.config/nvim/plugins')

" treesitter configurations
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" lsp configurations
Plug 'neovim/nvim-lspconfig'

" comments
Plug 'tpope/vim-commentary'

" fuzzy finder
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
call plug#end()

" lua interjection
lua << EOF

-- lsp attach hook definition
local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end
  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')
  local opts = { noremap=true, silent=true }
  buf_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap('n', 'K', '<cmd>lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  buf_set_keymap('n', '[[', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
  buf_set_keymap('n', ']]', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
end

local lspconfig = require('lspconfig')

-- lsp for python
lspconfig.pyright.setup{on_attach=on_attach}

lspconfig.pyls.setup{on_attach=on_attach}

-- treesitter config
require'nvim-treesitter.configs'.setup{
  incremental_selection = {
    enable = true,
    keymaps = {
      init_selection = "<CR>",
      node_incremental = "<CR>",
      node_decremental = "<BS>"
    },
  },
  highlight = {
    enable = true
  },
  indent = {
    enable = true
  },
  ensure_installed = {
      "python",
      "latex",
      "json",
      "html",
      "julia",
      "bash",
      "r"
    },
}

vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
    vim.lsp.diagnostic.on_publish_diagnostics, {
        -- disable virtual text on the right
        virtual_text = false,
        -- show signs
        signs = false,
        -- delay update diagnostics
        update_in_insert = false
    }
)

EOF

" telescope keybindings and highlight
nnoremap ;f <cmd>Telescope find_files theme=get_dropdown<cr>
nnoremap ;g <cmd>Telescope live_grep theme=get_dropdown<cr>
hi TelescopeSelection guibg=white guifg=black
hi TelescopeSelectionCaret guibg=white guifg=black
hi TelescopeMultiSelection guifg=yellow
highlight TelescopeMatching       guifg=blue
highlight TelescopePromptPrefix   guifg=red

" end of buffer (tilde filler) highlight
hi EndOfBuffer guifg=white

" general visual selection highlight
hi Visual guibg=#4f4f4f
hi VisualNOS guibg=#4f4f4f

" highlight popup menu
hi Pmenu guibg=purple
hi Pmenu guifg=white
hi LspDiagnosticsDefaultERror guifg=white

" highlight status line
hi StatusLine guibg=white
hi StatusLine guifg=black
hi StatusLineNC guibg=grey
hi StatusLineNC guifg=black

" highlight trailing whitespace
match Todo /\s\+$/
hi Todo guibg=white

